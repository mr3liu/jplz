# jplz

*jyut6 ping3 lin6 zaap6* provides [Jyutping](https://en.wikipedia.org/wiki/Jyutping) Cantonese pronunciation practice. It is built with [SvelteKit](https://kit.svelte.dev/) and [Tailwind CSS](https://tailwindcss.com/).

## Data sources

開放詞典 provided [a Jyutping dictionary for Chinese characters](https://github.com/kfcd/yyzd/blob/master/dist/md/繁體/粵語字典_(粵拼).md) whereas 粵語審音配詞字庫 provided [a frequency-sorted list of Chinese characters](https://humanum.arts.cuhk.edu.hk/Lexis/lexi-can/faq.php). I used them to produce the Jyutping dictionary used in *jplz* for, out of the 500 most commonly used Chinese characters, the 410 with unambiguous pronunciation.

## Deployment

Deployed to https://student.cs.uwaterloo.ca/~mr3liu/jplz/ with `adapter-static`.
