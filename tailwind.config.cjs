module.exports = {
  content: ['./src/**/*.{html,js,svelte,ts}'],
  theme: {
    extend: {
      fontFamily: {
        // reference: https://zh.wikisource.org/wiki/Template:楷體/style.css
        chinese: '"Kaiti TC", 標楷體, DFKai-SB, "AR PL UKai HK", "AR PL UKai TW", BiauKai, Kai, 全字庫正楷體, TW-Kai, "Kaiti SC", TH-Khaai-PP0, TH-Khaai-PP2, TH-Khaai-TP0, TH-Khaai-TP2, TH-Feon-A, 楷体, KaiTi, 楷体_GB2312, KaiTi_GB2312, FandolKai, 华文楷体, STKaiti, "AR PL UKai CN", EUDCKAI, cursive, 仿宋, fangsong, 宋体, serif, 黑体, sans-serif',
      },
      screens: {
        'hover-hover': {'raw': '(hover: hover)'},
      },
    },
  },
  plugins: [],
}
