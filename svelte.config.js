import adapter from '@sveltejs/adapter-static';
import preprocess from 'svelte-preprocess';

/** @type {import('@sveltejs/kit').Config} */
const config = {
	// Consult https://github.com/sveltejs/svelte-preprocess
	// for more information about preprocessors
	preprocess: preprocess(),

	kit: {
		prerender: {
			default: true
		},
		adapter: adapter(),
		paths: {
			base: '/~mr3liu/jplz'
		},
		vite: {
			ssr: {
				noExternal: '@fortawesome/free-solid-svg-icons'
			}
		}
	}
};

export default config;
